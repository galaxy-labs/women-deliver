// Gulp plugins:
var gulp = require('gulp'),
	sass = require('gulp-sass'),
	gulpFilter = require('gulp-filter'),
	gulpif = require('gulp-if'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	rimraf = require('gulp-rimraf'),
	modernizr = require('gulp-modernizr'),
	imagemin = require('gulp-imagemin'),
	cssmin = require('gulp-cssmin'),
	sftp = require('gulp-sftp'),

	newer = require('gulp-newer'),
	plumber = require('gulp-plumber'),

	neat = require('node-neat').includePaths,
	mainBowerFiles = require('main-bower-files'),
	runSequence = require('run-sequence'),
	args = require('yargs').argv,
	fs = require('fs');
	

// Path variables:

var base = {
	src:	'app/',	
	dist:	'dist/',
	temp:	'temp/',
	bower: 	'bower_components'
}

var srcPaths = {
	html: 	base.src + 'html/**/*',
	wp:		base.src + 'wp-theme/**/*',
	js: 	base.src + 'js/**/*.js',
	sass:	base.src + 'sass/**/*.+(scss|sass|less)',
	images: base.src + 'images/**/*.+(jpg|png|svg)',
	fonts: base.src + 'fonts/**/*'
}

var config = JSON.parse(fs.readFileSync('config.json'));
var min = args.min || false;
var upload = (args.to == "upload") ? true : false;



if( args.to in config.developers ) {
	base.dist = config.developers[args.to];
}



var distPaths = {
	css:	base.dist + 'css',
	fonts: 	base.dist + 'fonts',
	html:	base.dist,
	images: base.dist + 'images',
	js:		base.dist + 'js'
}


// Tasks:
gulp.task('watch', function() {
	
	gulp.watch(srcPaths.sass, ['sass']);
	gulp.watch(srcPaths.js, ['js']);
	gulp.watch(srcPaths.html, ['html'] );
	gulp.watch(srcPaths.wp, ['wp'] );
	gulp.watch(srcPaths.images, ['images'] )

}); 
 



gulp.task('default', ['watch'], function(cb) {
	
	if( upload ) {
		runSequence('clean','sass', 'icons', 'fonts', 'images', 'html', 'wp', 'js')
	}

	else {
		runSequence('clean', ['sass', 'icons', 'fonts', 'images', 'html', 'wp', 'js'])
	}
	
});

/*=============================
=            TOOLS            =
=============================*/

if( args.to in config ) {
	gulp.task('upload', function(){
		return gulp.src(base.dist + '**/*')
			.pipe(sftp({
			host: config.upload.host,
			user: config.upload.log,
			pass: config.upload.pass,
			remotePath: config.upload.dest
		}));
	});
}

gulp.task('clean', function(cb) {
	return gulp.src([base.dist, base.temp], { read: false }) // much faster
		.pipe(plumber())
		.pipe(rimraf({ force: true }))
});

gulp.task('images', function() {
	return gulp.src(srcPaths.images)
		.pipe(newer(distPaths.images))
		.pipe(gulpif(min, imagemin({
				progressive: true,
				optimizationLevel: 5
			})
		))
		.pipe(gulp.dest(distPaths.images))
		.pipe(gulpif(upload, sftp({
			host: config.upload.host,
			user: config.upload.log,
			pass: config.upload.pass,
			remotePath: config.upload.dest + 'images'
		})))
});

/*=====  End of TOOLS  ======*/


/*====================================
=            STYLES TASKS            =
====================================*/

gulp.task('sass', function() {

	return gulp.src(srcPaths.sass) 
		.pipe(newer(distPaths.css))
		.pipe(plumber())
	    .pipe(sass.sync({
	    	includePaths: [].concat(neat),
	    	outputStyle: 'expanded'
	    }))
	    .pipe(gulpif(min, cssmin()))
	    .pipe(plumber.stop())
	    .pipe(gulp.dest(distPaths.css))
	    .pipe(gulpif(upload, sftp({
			host: config.upload.host,
			user: config.upload.log,
			pass: config.upload.pass,
			remotePath: config.upload.dest + 'css'
		})))

});

gulp.task('fonts', function() {

	return gulp.src(srcPaths.fonts) 
		.pipe(newer(distPaths.fonts))
		.pipe(plumber())
	    .pipe(gulp.dest(distPaths.fonts))
	    .pipe(gulpif(upload, sftp({
			host: config.upload.host,
			user: config.upload.log,
			pass: config.upload.pass,
			remotePath: config.upload.dest + 'fonts'
		})))

});

gulp.task('icons', function() {
	return gulp.src(base.bower + '/font-awesome/fonts/**.*')
		.pipe(gulp.dest(distPaths.fonts))
		.pipe(gulpif(upload, sftp({
			host: config.upload.host,
			user: config.upload.log,
			pass: config.upload.pass,
			remotePath: config.upload.dest + 'fonts'
		})))
});


/*=====  End of STYLES TASKS  ======*/

/*==================================
=            HTML TASKS            =
==================================*/

gulp.task('html', function() {
	return gulp.src(srcPaths.html)
		.pipe(plumber())
		.pipe(newer(distPaths.html))
		.pipe(gulp.dest(distPaths.html))
		.pipe(gulpif(upload, sftp({
			host: config.upload.host,
			user: config.upload.log,
			pass: config.upload.pass,
			remotePath: config.upload.dest
		})))
});

gulp.task('wp', function() {
	return gulp.src(srcPaths.wp)
		.pipe(newer(distPaths.html))
		.pipe(gulp.dest(distPaths.html))
		.pipe(gulpif(upload, sftp({
			host: config.upload.host,
			user: config.upload.log,
			pass: config.upload.pass,
			remotePath: config.upload.dest
		})))
});

/*=====  End of HTML TASKS  ======*/

/*=====================================
=            SCRIPTS TASKS            =
=====================================*/

gulp.task('js', ['modernizr', 'bower', 'scripts'],  function() {
	return gulp.src(base.temp + '*.js')
		.pipe(concat('plugins.js'))
		.pipe(gulpif(min, uglify()))
		.pipe(gulp.dest(distPaths.js))
		.pipe(gulpif(upload, sftp({
			host: config.upload.host,
			user: config.upload.log,
			pass: config.upload.pass,
			remotePath: config.upload.dest + 'js'
		})))
});  
 
gulp.task('modernizr',  function() {
	return gulp.src(srcPaths.js)
		.pipe(modernizr({
			"options": [
				"setClasses"
			],
			"tests" : [ 
				'csstransforms', 
				'flexbox'
			],
			"crawl" : true
		}))
		.pipe(gulp.dest(base.temp))
})

gulp.task('bower', function() {
	return gulp.src(mainBowerFiles())
		.pipe(gulpFilter('*.js'))
		.pipe(gulp.dest(base.temp))
});

gulp.task('scripts', function() {
	return gulp.src(srcPaths.js)
		.pipe(newer(distPaths.js))
		.pipe(gulpif(min, uglify()))
		.pipe(gulp.dest(distPaths.js))
		.pipe(gulpif(upload, sftp({
			host: config.upload.host,
			user: config.upload.log,
			pass: config.upload.pass,
			remotePath: config.upload.dest + 'js'
		})))
});

/*=====  End of SCRIPTS TASKS  ======*/








