$(document).ready(function () {

	//Globbal variables
    var isMobile = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)),
    	$window = $(window),
        windowW = window.innerWidth,
        windowH = $(window).height(),
        scrollTop = 0;	  

    function globalEvents (argument) {
        var navDrop = $('.b-nav-drop'),
            navLink = $('.b-nav-link'),
            modal = $('.b-modal')
           
        var fixed = false,
            transitionEvent = "webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend",
            navPanel = $('.b-header'),
            st

        function mobileNav() {
            if(!fixed) {
                navPanel.addClass('fixed');

                fixed = true;
            }
        }   

         $window.on('scroll', function() {
            st = $(document).scrollTop();
            

            if( scrollTop > 480) {
                if(!fixed && st < scrollTop) {

                    navPanel.addClass('hidden').one(transitionEvent, function() {
                        navPanel.addClass('fixed').removeClass('hidden');
                    });
                    fixed = true;
                }
                if(fixed && st > scrollTop ) {
                    navPanel.addClass('hidden').one(transitionEvent, function() {
                        navPanel.removeClass('fixed hidden')
                    })
                    fixed = false;
                }
            }
            if( fixed && scrollTop < 480) {
                navPanel.addClass('hidden').one(transitionEvent, function() {
                    navPanel.removeClass('fixed hidden')
                })
                fixed = false;
            }

            scrollTop = $(document).scrollTop();
        })

        $window.on('resize', function() {
            windowW = window.innerWidth;
            windowH = $(window).height();
            if( $('body').is('.shaded')) {
                $('body').removeClass('shaded');
                navLink.removeClass('active');
                navDrop.removeClass('visible');
            }
            if( $('.b-filter-list').length && windowW > 768) {
                $('.b-filter-list li, .b-filter-sub-list').removeAttr('style');
                $('.b-filter-list li.open').removeClass('open')
            }
        }); 

        $('img.svg').each(function(){
            var $img = $(this),
                imgClass = $img.attr('class'),
                imgURL = $img.attr('src')

            $.get(imgURL, function(data) {
                var $svg = $(data).find('svg');

                if(typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass+' replaced-svg');
                }

                // Remove any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');

                $img.replaceWith($svg);

            }, 'xml');

        });
        
        $('body').on('click', function(event) {
            
            var triggers = [
                'search',
                'filter',
                'filter-stories',
                'filter-person'
            ],
            target = $(event.target)

           
           
            if (target.is('.b-filter-group-item') ) {
                
                target.siblings('ul').find('.b-filter-item').trigger('click')
                
                
            }
            if( target.is(navLink) && !target.is('.b-nav-link_search') ) {
                event.preventDefault();
                if(!target.is('.active')) {
                    navDropFit(target, target.parent().find(navDrop));

                    $('body').addClass('shaded');
                    target.addClass('active').parent().siblings().find(navLink).removeClass('active')
                    target.parent().find(navDrop).addClass('visible');
                    target.parent().siblings().find(navDrop).removeClass('visible');
                }
                else {
                    closeModal();
                } 
            }

            if( target.is(navDrop)) {
                return 
            }

            if( target.data('link') ) {
                var index = $.inArray(target.data('link'), triggers);

                if( index >= 0) {

                    $('body').addClass('deep-shaded');
                    if(!isMobile) {
                        $('.b-modal[data-id="'+triggers[index]+'"]').addClass('visible');
                    }
                    else {
                        console.log('test');
                        $('.b-modal[data-id="'+target.data('link')+'"]').addClass('visible');
                    }
                }
            }


            if(target.closest(modal).length ) {
                if(target.is('.b-modal-closer')) {
                    closeModal();
                    return
                }
                if(target.is('.b-filter-list > li')) {
                    mobileFilter(target);
                    return
                }
                else {

                    return
                }
            }
            if( navDrop.is('.visible') && !$(event.target).is(navLink) && !target.data('link')) {
                closeModal();
            }
            if(!isMobile) {
                if ( modal.is('.visible') && !$(event.target).is(navLink) && !target.data('link')) {
                    closeModal();
                }
            }
                
        

            //mobile
            if( target.is('.b-mobile-nav-btn')) {
                if(!$('.b-nav-mobile__back').is('visible')){
                    $('.b-nav-mobile__back').addClass('visible');
                }
                else {
                    $('.b-nav-mobile__back').removeClass('visible')
                }
            }
            if( target.is('.b-mobile-nav-closer a')) {
                $('.b-nav-mobile__back').removeClass('visible')
            }

            if(target.closest('.b-mobile-list__item.parent').length) {
                var item = target.closest('.parent');

                var subHeight
                if(target.is('.b-icon-closer ')) {
                    event.preventDefault();
                }
                if(!item.is('.open')) {
                    event.preventDefault();
                    subHeight = item.find('.b-mobile-sub-list').outerHeight();

                    item.addClass('open').find('.b-mobile-sub-menu').css('height', subHeight);

                    return
                }


                else {
                    item.removeClass('open').find('.b-mobile-sub-menu').css('height', '');
                }

                
            }

            //SIDEBAR

            if(target.is('.b-sidebar-list a')) {
                event.preventDefault();
                if(!target.is('.active')) {
                    target.addClass('active').parent().siblings().find('a').removeClass('active')
                }
                else {
                    $('.b-sidebar-list a').removeClass('active')
                }
            }
            
            function mobileFilter (item) {
                var amount = (windowW <= 480 ? 150 : 200);
                console.log(amount);

                if(!item.is('.open')){
                    $('.b-filter-list li.open').animate({
                        'height': "-="+amount
                    }, 300);

                    $('.b-filter-list li.open').removeClass('open');

                    item.animate({
                        'height': "+="+amount
                    }, 300, function(){
                        item.find('.b-filter-sub-list').fadeIn();
                    })
                    item.addClass('open')

                }
                else {
                    $('.b-filter-sub-list').fadeOut();

                    $('.b-filter-list li.open').animate({
                        'height': "-="+amount
                    }, 300, function () {
                        $('.b-filter-list li.open').removeClass('open')
                    })
                }
            }

            // LIKED

            if ( target.is('.b-btn_like') ) {
                $('.b-btn_like').toggleClass('like');
            }

            // SHARE

            if ( target.is('.b-btn_share') ) {
                $('.b-btn_share, .b-page-socials').toggleClass('open');
            }
                   
        });

        if(isMobile) {
            $('body').on('touchstart', function(event) {
               var target = $(event.target);
               if(modal.is('.visible') && !target.closest(modal).length ) {
                    closeModal();

               }
                /* Act on the event */
            });
        }

        function closeModal (argument) {
            $('body').removeClass('shaded deep-shaded');
            $('.b-modal').removeClass('visible');
            navLink.removeClass('active');
            navDrop.removeClass('visible');
        }

        function navDropFit (link, drop) {
            var offset = link.offset().left,
                dropWidth = drop.outerWidth()

            drop.css('width', dropWidth)
            
            if(offset + dropWidth / 2 >= windowW) {

                drop.css('left', windowW - dropWidth - 50)
                return
            }
            else {
                drop.css('left', offset - dropWidth / 4)
            }
        }

      
    }

    var gridContent = (function (argument) {
        var grid = $('.b-grid'),
            gutterVal,
            default_options = {},
            run = false,
            events = false,
            partial = false

        function init()  {
            
            if( grid.length ) {

                if ( grid.is('.b-grid_mixed')) {
                    masonryInit($('.b-grid_mixed'), {
                        itemSelector: '.b-grid-mixed__item',
                        columnWidth: 175,
                        gutter: 4
                    })
                   
                }

                if (grid.is('.b-grid_page')){
                    masonryInit($('.b-grid_page'), {
                        itemSelector: '.b-grid__item',
                        columnWidth: '.b-grid__item',
                        gutter: 2
                    })
                }
              
                if (grid.is('.b-grid_stories')){
                    masonryInit($('.b-grid_stories'), {
                        itemSelector: '.b-grid-story__item',
                        columnWidth: '.b-grid-story__item',
                        gutter: 2,
                        percentPosition: true
                    })

                }

                if (grid.is('.b-grid-staff')){
                    masonryInit($('.b-grid-staff'),{
                        itemSelector: '.b-grid-staff__item',
                        columnWidth: '.b-grid-staff__item.small',
                        gutter: 20,
                        percentPosition: true
                    });
                }

                if(grid.is('.b-grid-gif')) {
                    masonryInit($('.b-grid-gif'),{
                        itemSelector: '.b-grid-gif__item',
                        columnWidth: '.b-grid-gif__item.small',
                        gutter: 0,
                        percentPosition: true
                    });
                }

                if(grid.is('.b-grid_default')) {
                    console.log('initialize grid width default_options');

                    gutterVal = grid.width() * 2 / 100;
                    default_options = {
                        itemSelector: '.b-grid__item',
                        columnWidth: '.b-grid__item',
                        gutter: gutterVal,
                        percentPosition: true
                    }
                    masonryInit(grid, default_options);
                }

                run = true;
                eventHandlers();
                resize();
                

            }
        }

        function eventHandlers() {

            if(!events) {
                $window.on('resize', function () {
                    resize();
                })
                events = true
            }
        }

        function masonryInit(grid, options) {
            grid.masonry(options);
        }
        function masonryDestroy(grid) {
            grid.masonry('destroy');
        }
        function resize (argument) { 
            windowW = window.innerWidth

            
            
            if(windowW <= 978) {
                
               
                if(!partial) {
                    // console.log('partial masonry destroy');
                    $('.b-grid_stories, .b-grid_mixed, .b-grid-staff').masonry('destroy');
                    partial = true;
                }
                
                
                if(run) {
                    // console.log('full masonry destroy');

                    if(partial) {
                        masonryDestroy(grid.not('.b-grid_stories, .b-grid_mixed, .b-grid-staff'));
                    }   
                    else {
                        masonryDestroy(grid);
                    }
                    
                    run = false;

                }
                
            }

            else {

                if(!run) {
                    // console.log('reInitialize');
                    partial = false;
                    gridContent.init();
                    run = true;
                } 
            }
        }
        return {
            init: init
        }
    })() 

    var personGrid = (function() {
        var container = $('.b-grid-person, .b-staff-person, .b-grid_mixed, .b-person-grid'),
            item,
            drop,
            isAnimate,
            element,
            offset

        function init() {
            if(container.length) {
                item = $('.b-grid-person__item.b-person, .b-staff-person, .b-grid-mixed__item, .b-person-grid__item');
                
                events();

            }
        }

        function events() {
            $window.on('resize', function () {
                resize();
            })
            item.on('click', 'a', function(event) {
                event.preventDefault();

                if (!isAnimate) {
                    element = $(this).closest(item);
                    offset = $(this).parent().data('originalOffset');

                    if( !element.is('.open') ) {
                        element.addClass('open');

                        if(drop && drop.is('.visible')) {
                            element.removeClass('open')
                            hideDrop('show');
                            return
                        }
                        else {
                            showDrop(element,offset)
                        }
                        
                    }
                    else {
                        element.removeClass('open')
                        hideDrop();
                    }
                }
            });

            $('body').on('click', function(e) {
                var target = $(e.target);

                if(!target.closest(item).length && !target.closest('.b-person-dropdown.visible').length) {
                    hideDrop();
                }
            })

            $('.b-person-info__closer').on('click', function() {
                item.removeClass('open')
                hideDrop();
            });
            resize();
        }
        function showDrop(el, offset) {
            isAnimate = true;
            var focusOffset = (windowW > 768 ? (offset - 200) : (offset - 50));

            if(!el.attr('id')) {
                
                drop = $( '[data-person="test"]' );
            }
            else {
                
                drop = $( '[data-person="' + el.attr('id') +'"]' );
            }
            
            // Add space
            $('body').addClass('light-shaded');
            el.css('marginBottom', drop.outerHeight() + 4);

            
            // Get position and show
            drop.css('top', offset).addClass('visible')
            el.siblings().removeClass('open').css('margin', '');

            $('body').animate({
                scrollTop: focusOffset
            }, 1000, function () {
                isAnimate = false
            })
            
            
          
        }

        function hideDrop(callback) {
            isAnimate = true
            drop.removeClass('visible');
            item.css('margin', '');
            $('body').removeClass('light-shaded');
            
          
            if(callback === 'show') {
             
                setTimeout(function () {
                    showDrop(element,offset);
                },500)
                    
            }
            if( callback === 'resize') {
                setTimeout(function () {
                    item.removeClass('open');
                    calculateOffset();

                },500)
            }
            else {
                isAnimate = false
            }
           
        }
        function resize() {
            if( item.closest('.open').length) {
                hideDrop('resize');
            }
            else {
                calculateOffset();
            }

        }
        function calculateOffset() {
            var margin = (item.is('.b-staff-person')) ? 19 : 2;
            item.each(function() {
                // console.log($(this).offset().top + $(this).outerHeight() + margin);
                $(this).attr('data-original-offset', $(this).offset().top + $(this).outerHeight() + margin )
            })
            isAnimate = false
        }

        return {
            init: init
        }
    })()

    function reorderElements () {

        var container = $('.reorder'),
            zeroElement = $('[data-order="0"]'),
            reordered = false,
            breakpoint = 978,
            cloned = false
           

        if(container.length) {
            windowW = $(window).width()
            firstLoad(container);

            $window.on('resize', function () {
           
                if( windowW <= breakpoint ) {
                    hideElements(zeroElement);

                    newOrder(container);
                }
               

                if(windowW > breakpoint) {
                    defaultOrder(container);
                    showElements(zeroElement);
                }
            })
        }

        function makeClone() {
           
            console.log('make clones');
            container.each(function(index, el) {
                $(this).clone().addClass('clone').appendTo($(this).parent()).hide();
            });
            cloned = true
            
                
        }

        function firstLoad (container) {
          
            if(windowW <= breakpoint) {
                newOrder(container);
            }
        }
            

        function newOrder (container) {
            if(!reordered)  {
                console.log('new order');

                if(!cloned) {
                    makeClone();
                    
                    container.each(function (e) {
                        var item = $(this).find('[data-order]').addClass('reordered');
                        item.appendTo($(this).parent());
                    }); 

                    container.parent().each(function(index, el) {
                        $(this).children().sort(function(a, b) {
                            return + a.dataset.order - + b.dataset.order;
                        }).appendTo($(this));
                    })
                }
                else {
                    container.parent().each(function(index, el) {
                        $(this).children().not('.clone').show();
                        $(this).find('.clone').hide();
                    })
                }
                
                
                reordered = true;
                container = $('.reorder');

               
            }
        }
        function defaultOrder (container) {
            if(reordered) {
                console.log('default order');

                container.parent().each(function (e) {
                    $(this).children().not('.clone').hide();
                    $(this).find('.clone').show();
                })
               
                reordered = false;
            }
                
        }

        function hideElements (el) {
            if(el.length) {
                el.each(function () {
                    $(this).hide();
                })
            }
        }

        function showElements(el) {
            if(el.length) {
                el.each(function () {
                    $(this).show();
                })
            }
        }

        
    }

    function globalInit() {
		globalEvents();
        gridContent.init();
        reorderElements();
        personGrid.init();
	}
	globalInit();
});