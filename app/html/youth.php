<?php @include ('blocks/header.php'); ?>

<section class="b-page-top b-page-top_extended" style="background-image: url('images/top-page-youth.jpg');">
	<div class="l-top-inner">
		<h1>Youth</h1>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id dolor id nibh ut id elit duis mollis est non luctus, nisi erat donec sed odio dui.</p>
	</div>
</section>

<div class="l-content l-content_with-sidebar">

	<aside class="l-content-col-3 b-page-sidebar">
		<?php @include ('blocks/sidebar-list.php'); ?>
	</aside>
	
	<div class="l-content-col-9 b-grid b-grid_mixed">
		<div class="b-grid-mixed__item empty"></div>
		<div class="b-grid-mixed__item b-article">
			<img class="b-article__bg" src="images/article-item.jpg" alt="">
			<div class="b-article__text on-dark">
				<h3>Consectetur adipiscing </h3>
			</div>
		</div>
		<div class="b-grid-mixed__item b-person">
			<img src="images/p-collins.jpg" alt="">
			<a class="b-person-link">
				<span class="b-person-name" >Collins </span>
			</a>
		</div>
		<div class="b-grid-mixed__item b-person">
			<img src="images/p-chiamaka.jpg" alt="">
			<a href="" class="b-person-link">
				<span class="b-person-name">Chiamaka Uzomba</span>
			</a>
		</div>
		<div class="b-grid-mixed__item b-person">
			<img src="images/p-chansolinda.jpg" alt="">
				<a href="" class="b-person-link">
				<span class="b-person-name">Chansolinda</span>
			</a>
		</div>
		<div class="b-grid-mixed__item b-article b-article_video">
			<a href="" class="b-article-video-btn"></a>
			<img class="b-article__bg" src="images/article-video.jpg" alt="">
			<div class="b-article__text on-dark">
				<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
			</div>
			<div class="b-article__overlay"></div>
		</div>
		<div class="b-grid-mixed__item b-featured-person">
			<a href="" class="b-person-link">
				<img src="images/p-Adebisi-extended.jpg" alt="">
				<div class="b-featured-person-info right">
					<span class="b-person-name">Adebisi Adenipekun</span>
					<span class="b-person-info">Quick metric or standout highlight </span>
				</div>
			</a>
		</div>
		<div class="b-grid-mixed__item b-person">
			<img src="images/p-dakshitha.jpg" alt="">
				<a href="" class="b-person-link">
				<span class="b-person-name">Dakshitha</span>
			</a>
		</div>
		<div class="b-grid-mixed__item b-person">
			<img src="images/p-adriana.jpg" alt="">
				<a href="" class="b-person-link">
				<span class="b-person-name">Adriana</span>
			</a>
		</div>
		<div class="b-grid-mixed__item b-person">
			<img src="images/p-angela.jpg" alt="">
				<a href="" class="b-person-link">
				<span class="b-person-name">Angela</span>
			</a>
		</div>
		<div class="b-grid-mixed__item b-person">
			<img src="images/p-boris.jpg" alt="">
				<a href="" class="b-person-link">
				<span class="b-person-name">Boris</span>
			</a>
		</div>
		<div class="b-grid-mixed__item b-article b-article_image">
			<div class="l-article-inner">
				<img src="images/article-img2.png" alt="">
			</div>
		</div>
		<div class="b-grid-mixed__item empty"></div>
		<div class="b-grid-mixed__item b-featured-person">
			<a href="" class="b-person-link">
				<img src="images/p-Aishoola-extended.jpg" alt="">
				<div class="b-featured-person-info left">
					<span class="b-person-name">Aishoola Aisaeva</span>
					<span class="b-person-info">Quick metric or standout highlight </span>
				</div>
					
			</a>
		</div>
		<div class="b-grid-mixed__item b-article b-article_audio">
			<a href="" class="b-article-audio-btn"></a>
			<img class="b-article__bg" src="images/article-audio.jpg" alt="">
			<div class="b-article__text on-dark">
				<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
			</div>
			<div class="b-article__overlay"></div>
		</div>
		<div class="b-grid-mixed__item b-person">
			<img src="images/p-danica.jpg" alt="">
				<a href="" class="b-person-link">
				<span class="b-person-name">Danica</span>
			</a>
		</div>
		<div class="b-grid-mixed__item b-person">
			<img src="images/p-sofia.jpg" alt="">
				<a href="" class="b-person-link">
				<span class="b-person-name">Sofia</span>
			</a>
		</div>
		<div class="b-grid-mixed__item b-person">
			<img src="images/p-blessing.jpg" alt="">
				<a href="" class="b-person-link">
				<span class="b-person-name">Blessing</span>
			</a>
		</div>
		<div class="b-grid-mixed__item b-person">
			<img src="images/p-umair.jpg" alt="">
				<a href="" class="b-person-link">
				<span class="b-person-name">Umair</span>
			</a>
		</div>
		<div class="b-grid-mixed__item b-person">
			<img src="images/p-salam.jpg" alt="">
				<a href="" class="b-person-link">
				<span class="b-person-name">Salam</span>
			</a>
		</div>
		<div class="b-grid-mixed__item b-featured-person">
			<a href="" class="b-person-link">
				<img src="images/p-Steven-extended.jpg" alt="">
				<div class="b-featured-person-info right on-dark">
					<span class="b-person-name">Steven Twinomugisha</span>
					<span class="b-person-info">Quick metric or standout highlight </span>
				</div>
			</a>
		</div>
		<div class="b-grid-mixed__item b-person">
			<img src="images/p-Hanifatur.jpg" alt="">
				<a href="" class="b-person-link">
				<span class="b-person-name">Hanifatur</span>
			</a>
		</div>
		<div class="b-grid-mixed__item b-person">
			<img src="images/p-Franklin.jpg" alt="">
				<a href="" class="b-person-link">
				<span class="b-person-name">Franklin</span>
			</a>
		</div>
		<div class="b-grid-mixed__item b-person">
			<img src="images/p-temesgen.jpg" alt="">
				<a href="" class="b-person-link">
				<span class="b-person-name">Temesgen</span>
			</a>
		</div>
		<div class="b-grid-mixed__item b-person">
			<img src="images/p-sona.jpg" alt="">
				<a href="" class="b-person-link">
				<span class="b-person-name">Sona</span>
			</a>
		</div>
		<div class="b-grid-mixed__item b-person">
			<img src="images/p-Nehsuh.jpg" alt="">
				<a href="" class="b-person-link">
				<span class="b-person-name">Nehsuh</span>
			</a>
		</div>
		<div class="b-grid-mixed__item button">
			<div class="l-article-inner">
				<a href="" class="b-btn b-btn_border b-btn_blue">MEET ALL THE YOUNG LEADERS <img src="images/uparrow.svg" class="svg"></a>
			</div>
		</div>
	</div>
</div>

<section class="b-page-bottom b-page-bottom_type-c">
	<div class="l-content">
		<div class="b-socials b-socials_large">
			<img src="images/bg-socials_w.jpg" alt="">
			<div class="b-socials-col">
				<?php @include('blocks/form-join.php') ?>
			</div>
			<div class="b-socials-col">
				<?php @include('blocks/socials.php') ?>
			</div>
			
		</div>
	</div>
</section>
<?php @include ('blocks/person-info.php'); ?>
<?php @include ('blocks/footer.php'); ?>