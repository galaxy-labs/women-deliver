<?php @include ('blocks/header.php'); ?>

<section class="b-page-img b-page-img_detail" style="background-image: url('images/top-page_issues-detail.jpg');"></section>
<section class="b-page-content">
    <div class="b-page-content__inner">
        
        <?php @include ('blocks/breadcrumbs.php'); ?>
        <h2>Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua.</h2>
        <h3>THIS IS A SUBTITLE (OPTIONAL) Maecenas faucibus mollis interdum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa.</h3>
        <div class="b-page-cat">
            <span class="b-page-cat__bold">Sexual Reproductive Health & Rights</span>
            <span class="b-page-cat__bold">Non Communicable Diseases</span>
            <span class="b-page-cat__bold">Comprehensive Health Services</span>
        </div>
        <div class="b-page-btn">
            <div class="b-btn_share"><span>share</span>
                <div class="b-page-socials">
                    <a href="" class="b-social-link b-social-link_fb"></a>
                    <a href="" class="b-social-link b-social-link_tw"></a>
                    <a href="" class="b-social-link b-social-link_pint"></a>
                    <a href="" class="b-social-link b-social-link_inst"></a>
                    <a href="" class="b-social-link b-social-link_google"></a>
                    <a href="" class="b-social-link b-social-link_in"></a>
                    <a href="" class="b-social-link b-social-link_mail"></a>
                </div>
            </div> 
            
            <a class="b-btn_like b-btn_donate"><span>like</span><span class="like">liked</span></a>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. .</p>
    </div>
    <img src="images/page-img-inner1.jpg" alt="img">
    <div class="b-page-content__inner">
         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    </div>
</section>
<section class="b-page-img" style="background-image: url('images/page-img-inner2.jpg');"></section>
<section class="b-page-content">
    <div class="b-page-content__inner">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <div class="b-block-img">
            
            <img src="images/page-img-inner3.jpg" alt="img">
            <p>Dolor ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna </p>
        </div>
            
        <p>Ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laboroident, sunt in culpa qui officia deserunt molum.</p>
        
        <p>Fugiat ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
    </div> 
</section>
<section class="b-page-bottom b-page-bottom_type-b">
    <div class="l-content">
        <div class="l-content__left">
            <div class="b-page-cat">
                <span class="b-page-cat__bold">Sexual Reproductive Health & Rights</span>
            </div>
            <div class="b-grid b-grid_page">
                <div class="b-grid__item b-article b-article_video">
                    <a href="" class="b-article-video-btn"></a>
                    <img class="b-article__bg" src="images/article-video.jpg" alt="">
                    <div class="b-article__text on-white">
                        <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
                    </div>
                </div>
                <div class="b-grid__item b-article">
                    <img class="b-article__bg" src="images/article-item.jpg" alt="">
                    <div class="b-article__text on-white">
                        <h3>Consectetur adipiscing </h3>
                    </div>
                </div>
            </div>
            <div class="l-content-col-main">
                <div class="b-socials b-socials_small">
                    <img src="images/bg-socials_s.jpg" alt="">
                    <div class="b-socials__inner">
                        <?php @include('blocks/socials.php') ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="l-content__right">
            <?php @include('blocks/top-stories.php'); ?>
        </div>
    </div>
</section>

<?php @include ('blocks/footer.php'); ?>