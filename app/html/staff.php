<?php @include ('blocks/header.php'); ?>

<div class="l-content l-content_with-sidebar b-staff-page">

	<aside class="l-content-col-3 b-page-sidebar">
		<?php @include ('blocks/sidebar-list_general.php'); ?>
	</aside>
	<div class="l-content-col-9 b-grid b-grid-staff">

        <!-- row -->

		<div class="b-grid-staff__item b-staff-person large" id="Katja_Iverson">
			<a  class="b-staff-person-link-wrap">
				<img src="images/staff-person_katja.jpg" alt="">
				<div class="b-staff-person-label">
					<span class="b-staff-person-label__name">Katja Iverson</span>
					<span class="b-staff-person-label__position">Chief Executive Officer</span>
				</div>
			</a>
		</div>
		<div class="b-grid-staff__item b-staff-person small">
			<a href="" class="b-staff-person-link-wrap">
				<div class="b-staff-person-label">
					<span class="b-staff-person-label__name">Juliana Bennington</span>
					<span class="b-staff-person-label__position">Youth Program and Advocacy Intern</span>
				</div>
			</a>
		</div>
        <div class="b-grid-staff__item b-staff-person small">
            <a href="" class="b-staff-person-link-wrap">
                <div class="b-staff-person-label">
                    <span class="b-staff-person-label__name">Kelsi Boyle</span>
                    <span class="b-staff-person-label__position">Communications Manager</span>
                </div>
            </a>
        </div>

        <!-- row -->
        
        <div class="b-grid-staff__item b-staff-person small">
            <a href="" class="b-staff-person-link-wrap">
                <div class="b-staff-person-label">
                    <span class="b-staff-person-label__name">Maria DeVoe</span>
                    <span class="b-staff-person-label__position">Associate, Policy and Advocacy</span>
                </div>
            </a>
        </div>

        <div class="b-grid-staff__item b-staff-person large">
            <a href="" class="b-staff-person-link-wrap">
                <img src="" alt="">
                <div class="b-staff-person-label">
                    <span class="b-staff-person-label__name">Jill Sheffield</span>
                    <span class="b-staff-person-label__position">Board Member and President</span>
                </div>
            </a>
        </div>
        

        <div class="b-grid-staff__item b-staff-person small">
            <a href="" class="b-staff-person-link-wrap">
                <div class="b-staff-person-label">
                    <span class="b-staff-person-label__name">Tatiana DiLanzo</span>
                    <span class="b-staff-person-label__position">Advocacy and Policy Intern</span>
                </div>
            </a>
        </div>

        
        <!-- row -->

        <div class="b-grid-staff__item b-staff-person small">
            <a href="" class="b-staff-person-link-wrap">
                <div class="b-staff-person-label">
                    <span class="b-staff-person-label__name">Rachel Larkin</span>
                    <span class="b-staff-person-label__position"> Conference Program Support Intern</span>
                </div>
            </a>
        </div>

        <div class="b-grid-staff__item b-staff-person small">
            <a href="" class="b-staff-person-link-wrap">
                <div class="b-staff-person-label">
                    <span class="b-staff-person-label__name">Louise Dunn</span>
                    <span class="b-staff-person-label__position">Director of Finance and Administration</span>
                </div>
            </a>
        </div>
        <div class="b-grid-staff__item b-staff-person small">
            <a href="" class="b-staff-person-link-wrap">
                <div class="b-staff-person-label">
                    <span class="b-staff-person-label__name">Scarlet Macas</span>
                    <span class="b-staff-person-label__position">Finance and Office Manager</span>
                </div>
            </a>
        </div>

        <!-- row -->

        <div class="b-grid-staff__item b-staff-person small">
            <a href="" class="b-staff-person-link-wrap">
                <div class="b-staff-person-label">
                    <span class="b-staff-person-label__name">Alyssa Mahoney</span>
                    <span class="b-staff-person-label__position">Events and Projects Manager</span>
                </div>
            </a>
        </div>

        <div class="b-grid-staff__item b-staff-person male small">
            <a href="" class="b-staff-person-link-wrap">
                <div class="b-staff-person-label">
                    <span class="b-staff-person-label__name">Roger Pagano</span>
                    <span class="b-staff-person-label__position">Office Administrator</span>
                </div>
            </a>
        </div>
        <div class="b-grid-staff__item b-staff-person small">
            <a href="" class="b-staff-person-link-wrap">
                <div class="b-staff-person-label">
                    <span class="b-staff-person-label__name">Jessica Malter</span>
                    <span class="b-staff-person-label__position">Strategic Communications Director</span>
                </div>
            </a>
        </div>
	</div>
</div>

<section class="b-page-bottom b-page-bottom_type-b">
    <div class="l-content">
        <div class="l-content__left">
           
            <div class="b-grid b-grid_page">
                <a class="b-grid__item b-cat">

                    <img class="b-cat__bg" src="images/article-item16.jpg" alt="">
                    <h3 class="b-cat__label mage">Board Members</h3>
                </a>
                <a class="b-grid__item b-cat">
                    <img class="b-cat__bg" src="images/article-item20.jpg" alt="">
                    <h3 class="b-cat__label green">Staff</h3>
                </a>
            </div>
            <div class="l-content-col-main">
                <div class="b-socials b-socials_small">
                    <img src="images/bg-socials_s.jpg" alt="">
                    <div class="b-socials__inner">
                        <?php @include('blocks/socials.php') ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="l-content__right">
            <?php @include('blocks/top-stories.php'); ?>
        </div>
    </div>
</section>

<?php @include ('blocks/person-info.php'); ?>
<?php @include ('blocks/footer.php'); ?>