<?php @include ('blocks/header.php'); ?>

<section class="b-page-top b-page-top_youth" style="">
	<div class="l-top-inner">
		<h1>Featured Stories</h1>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id dolor id nibh ut id elit duis mollis est non luctus, nisi erat donec sed odio dui.</p>
	</div>
</section>


<div class="l-content l-content_with-sidebar">

	<aside class="l-content-col-3 b-page-sidebar">
		<?php @include ('blocks/sidebar-list.php'); ?>
	</aside>
	<form class="b-grid-filter-search">
		<input type="text" placeholder="Filter">
		<a data-link="filter-stories"></a>
	</form>
	<div class="l-content-col-9 b-grid b-grid_stories">
		
		<div class="b-grid-story__item b-article">
			<a href="">
				<img class="b-article__bg" src="images/article-item.jpg" alt="">
				<div class="b-article__text on-dark">
					<h3>Consectetur adipiscing </h3>
				</div>
			</a>
				
		</div>
		<div class="b-grid-story__item b-article b-article_video">
			<a href="" class="b-article-video-btn"></a>
			<img class="b-article__bg" src="images/article-video.jpg" alt="">
			<div class="b-article__text on-dark">
				<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
			</div>
			<div class="b-article__overlay"></div>
		</div>
		<div class="b-grid-story__item empty"></div>
		<div class="b-grid-story__item b-article">
			<a href="">
				<img class="b-article__bg" src="images/article-item8.jpg" alt="">
				<div class="b-article__text on-dark">
					<h3>Lorem ipsum dolor sit amet, consectetur ...</h3>
				</div>
			</a>
		</div>
		<div class="b-grid-story__item b-article large">
			<a href="">
				<img class="b-article__bg" src="images/article-item9.jpg" alt="">
				<div class="b-article__text on-dark">
					<h3>Donec ullamcorper nulla non metus auctor porta ac consectetur ac, vestibulum ...</h3>
				</div>
			</a>
		</div>
		<div class="b-grid-story__item b-article b-article_audio">
			<a href="" class="b-article-audio-btn"></a>
			<img class="b-article__bg" src="images/article-audio.jpg" alt="">
			<div class="b-article__text on-dark">
				<h3>Consectetur adipiscing </h3>
			</div>
			<div class="b-article__overlay"></div>
		</div>
		<div class="b-grid-story__item empty"></div>
		<div class="b-grid-story__item b-article">
			<a href="">
				<img class="b-article__bg" src="images/article-item10.jpg" alt="">
				<div class="b-article__text on-dark">
					<h3>Sed posuere consectet ur est at lobortis ...</h3>
				</div>
			</a>
		</div>
		<div class="b-grid-story__item b-article">
			<a href="">
				<img class="b-article__bg" src="images/article-item11.jpg" alt="">
				<div class="b-article__text on-dark">
					<h3>Consectetur adipiscing </h3>
				</div>
			</a>
		</div>
		<div class="b-grid-story__item b-article">
			<a href="">
				<img class="b-article__bg" src="images/article-item12.jpg" alt="">
				<div class="b-article__text on-dark">
					<h3>Consectetur adipiscing </h3>
				</div>
			</a>
		</div>
		<div class="b-grid-story__item b-article b-article_audio">
			<a href="" class="b-article-audio-btn"></a>
			<img class="b-article__bg" src="images/article-item13.jpg" alt="">
			<div class="b-article__text on-dark">
				<h3>Sed posuere consectet ur est at lobortis ...</h3>
			</div>
			<div class="b-article__overlay"></div>
		</div>
		<div class="b-grid-story__item b-article">
			<a href="">
				<img class="b-article__bg" src="images/article-item14.jpg" alt="">
				<div class="b-article__text on-dark">
					<h3>Consectetur adipiscing </h3>
				</div>
			</a>
		</div>
		<div class="b-grid-story__item b-article b-article_video">
			<a href="" class="b-article-video-btn"></a>
			<img class="b-article__bg" src="images/article-item16.jpg" alt="">
			<div class="b-article__text on-dark">
				<h3>Lorem ipsum dolor sit amet, consectetur ...</h3>
			</div>
			<div class="b-article__overlay"></div>
		</div>
		<div class="b-grid-story__item b-article">
			<a href="">
				<img class="b-article__bg" src="images/article-item17.jpg" alt="">
				<div class="b-article__text on-dark">
					<h3>Consectetur adipiscing </h3>
				</div>
			</a>
		</div>
		<div class="b-grid-story__item empty"></div>
		<div class="b-grid-story__item b-article">
			<a href="">
				<img class="b-article__bg" src="images/article-item18.jpg" alt="">
				<div class="b-article__text on-dark">
					<h3>Lorem ipsum dolor sit amet, consectetur ...</h3>
				</div>
			</a>
		</div>
		<div class="b-grid-story__item b-article b-article_video large">
			<a href="" class="b-article-video-btn"></a>
			<img class="b-article__bg" src="images/article-item_big3.jpg" alt="">
			<div class="b-article__text on-dark">
				<h3>Donec ullamcorper nulla non metus auctor porta ac consectetur ac, vestibulum ...</h3>
			</div>
			<div class="b-article__overlay"></div>
		</div>
		<div class="b-grid-story__item b-article b-article_audio">
			<a href="" class="b-article-audio-btn"></a>
			<img class="b-article__bg" src="images/article-item19.jpg" alt="">
			<div class="b-article__text on-dark">
				<h3>Sed posuere consectet ur est at lobortis ...</h3>
			</div>
			<div class="b-article__overlay"></div>
		</div>
	</div>
</div>

<section class="b-page-bottom b-page-bottom_type-c">
	<div class="l-content">
		<div class="b-socials b-socials_large">
			<img src="images/bg-socials_w.jpg" alt="">
			<div class="b-socials-col">
				<?php @include('blocks/form-join.php') ?>
			</div>
			<div class="b-socials-col">
				<?php @include('blocks/socials.php') ?>
			</div>
			
		</div>
	</div>
</section>

<?php @include ('blocks/footer.php'); ?>
