<?php @include ('blocks/header.php'); ?>

<section class="b-page-top b-page-top_works">
	<div class="b-page-top__bg">
		<img  src="images/top-page-our-work.jpg" alt="">
	</div>
	
	<div class="b-page-top__title">
		<a href=""><img src="images/uparrow.svg" class="svg" alt=""></a>
		<p>As a leading, global advocate for girls’ and women’s health, rights, and wellbeing, Women Deliver brings together diverse voices and interests to drive progress, particularly in maternal, sexual, and reproductive health and rights. We build capacity, share solutions, and forge partnerships to spark political commitment and investment in girls and women. We believe that when the world invests in girls and women, everybody wins.</p>
	</div>
</section>
 
<section class="b-work-row b-work-row_img-grid">

	<div class="l-flex-col">
		<div class="b-work-staff">
			<img src="images/work-img_staff.jpg" alt="">
			<div class="b-work-staff__content">
				<p>Back in 2007, despite promises and disjointed efforts, the rate of maternal deaths was at an atrocious level. World leaders needed to come together, and they needed a place to do it. Jill Sheffield, a season maternal health activist created Women Deliver, a conference focused exclusively on solutions and action. Since those early days, Women Deliver has developed into much more than our triennial conference. </p>
				<ul class="b-work-staff__btn-set">
					<li><a href="" class="b-btn b-btn_green b-btn_solid">STAFF</a></li>
					<li><a href="" class="b-btn b-btn_green b-btn_solid">BOARD</a></li>
					<li><a href="" class="b-btn b-btn_green b-btn_solid">PARTNERS</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="l-flex-col b-desktop-content"> 
		<img class="b-work-responsive-image" src="images/work-img_staff2.jpg" alt="">
	</div> 
</section>
<section class="b-work-row"> 
	<div class="b-work-row__text">
		<div class="b-row-text-inner">
			<img src="images/work-row_convening.png" alt="" class="b-work-row-icon">
			<h2 class="b-work-row-header">Convening</h2>
			<p>Through our extensive network and reach, we create the physical and virtual spaces for diverse voices and groups to meet, build coalitions, and drive progress.</p>
			<p><small>Women Deliver fills a unique niche within the wider women’s empowerment movement, health industry, and global development space. We work with a wide set of partners – UN agencies, governments, private sector, civil society, media, and young people – to uncover solutions that deliver the most impact for girls and women.</small></p>
		</div>
	</div>
</section>
<section class="b-work-row b-work-row_img-grid">
	<div class="l-flex-col l-flex-col-6">
		<div class="b-work-event">
			<img src="images/work-row_conference.jpg" alt="">
			<div class="b-work-event-info">
				<h2>4th Global Conference</h2>
				<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras mattis consectetur purus sit amet fermentum. Maecenas faucibus mollis interdum. Donec ullamcorper nulla non metus auctor fringilla Nulla vitae elit libero.</p>
				<a href="" class="b-btn b-btn_border b-btn_green"><img src="images/uparrow.svg" alt="" class="svg">Learn More</a>
			</div>
		</div>
	</div>
	<div class="l-flex-col l-flex-col-4">
		<div class="b-work-event-video-callout">
			<a href="" class="b-video-callout-btn"></a>
			<img src="images/work-img_video-poster.jpg" alt="">
		</div>
		<div class="b-work-event-link-bar">
			<a href="" class="b-btn b-btn_get-extended">
				<span class="b-btn__icon b-btn__icon_pdf">&#xf1c1;</span>
				<img src="images/uparrow.svg" alt="" class="svg">
				DOWNLOAD THE PRESS RELEASE
			</a>
		</div>
	</div>
</section>
<section class="b-work-row">
	<div class="b-work-row__text">
		<div class="b-row-text-inner">
			<img src="images/work-row_engaging.png" alt="" class="b-work-row-icon">
			<h2 class="b-work-row-header">ENGAGING</h2>
			<p>The <strong>investment case</strong> is a strong one, and we ensure it is spread far and wide. We collect evidence, create effective communication tools, and disseminate messages to influence agendas and fuel the movement.</p>
		</div>
	</div> 
	<div class="b-work-row__inner-grid">
		<div class="b-flex-grid">
			<div class="b-flex-grid__item b-article">
				<a href="">
					<img class="b-article__bg" src="images/article-item14.jpg" alt="">
					<div class="b-article__text on-dark">
						<h3>Katja’s latest speaking engagement </h3>
					</div>
				</a>
			</div>
			<div class="b-flex-grid__item b-article">
				<a href="">
					<img class="b-article__bg" src="images/article-item21.jpg" alt="">
					<div class="b-article__text on-dark">
						<h3>View our Publications</h3>
					</div>
				</a>
			</div>
			<div class="b-flex-grid__item b-flex-grid__item_fit">

				<?php @include('blocks/socials.php') ?>
			</div>
			<div class="b-flex-grid__item b-flex-grid__item_full">
				<h3>Deliver for Good campaign banner <span>FPO</span></h3>
			</div>
		</div>
	</div>
</section>
<section class="b-work-row">
	<div class="b-work-row__text b-work-row__text_on-background">
		<img class="b-work-responsive-image" src="images/work-row_leaders.jpg" alt="">
		<div class="b-row-text-inner b-row-text-inner_white">
			<img src="images/work-row_cultivating.png" alt="" class="b-work-row-icon">
			<h2 class="b-work-row-header">CULTIVATING</h2>
			<p>Young people are the best spokespeople for their own needs and powerful agents of change. Through our extensive <strong>Youth Initiative</strong>, we build the skills of young people and find platforms for them to share their voices and experiences.</p>
		</div>
	</div>
	<div class="l-flex ">
		<div class="l-flex-col l-flex-col-6 l-flex-v-center b-youth-min">
			<div class="b-youth-min__title">
				<h2>Young Leaders</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id dolor id nibh ut id elit duis mollis est non luctus, nisi erat donec sed odio dui.</p>
			</div>
			<ul class="b-youth-min__grid b-flex-grid">
				<li class="b-flex-grid__item_1of3 b-person"><img src="images/p-collins.jpg" alt="">
					<a href="" class="b-person-link"><span class="b-person-name">Collins</span></a>
				</li>
				<li class="b-flex-grid__item_1of3 b-person"><img src="images/p-chiamaka.jpg" alt="">
					<a href="" class="b-person-link"><span class="b-person-name">Chiamaka Uzomba</span></a>
				</li>
				<li class="b-flex-grid__item_1of3 b-person"><img src="images/p-chansolinda.jpg" alt="">
					<a href="" class="b-person-link"><span class="b-person-name">Chansolinda</span></a>
				</li>
				<li class="b-flex-grid__item_1of3 b-person"><img src="images/p-dakshitha.jpg" alt="">
					<a href="" class="b-person-link"><span class="b-person-name">Dakshitha</span></a>
				</li>
				<li class="b-flex-grid__item_1of3 b-person"><img src="images/p-adriana.jpg" alt="">
					<a href="" class="b-person-link"><span class="b-person-name">Adriana</span></a>
				</li>
				<li class="b-flex-grid__item_1of3 b-person"><img src="images/p-angela.jpg" alt="">
					<a href="" class="b-person-link"><span class="b-person-name">Angela</span></a>
				</li>
				<li class="b-flex-grid__item_1of3 b-person"><img src="images/p-boris.jpg" alt="">
					<a href="" class="b-person-link"><span class="b-person-name">Boris</span></a>
				</li>
				<li class="b-flex-grid__item_1of3 b-person"><img src="images/p-danica.jpg" alt="">
					<a href="" class="b-person-link"><span class="b-person-name">Danica</span></a>
				</li>
				<li class="b-flex-grid__item_1of3 b-person"><img src="images/p-sofia.jpg" alt="">
					<a href="" class="b-person-link"><span class="b-person-name">Sofia</span></a>
				</li>
			</ul>
			<a href="" class="b-btn b-btn_border b-btn_blue">VIEW ALL THE YOUNG LEADERS <img src="images/uparrow.svg" alt="" class="svg"></a>
		</div>
		<div class="l-flex-col l-flex-col-4 l-flex l-flex_v-cells b-work-row-photo-set">
			<div class="l-flex-cell">
				<img class="b-work-responsive-image" src="images/work-img_photo1.jpg" alt="">
			</div>
			<div class="l-flex-cell b-desktop-content">
				<img class="b-work-responsive-image" src="images//work-img_photo2.jpg" alt="">
			</div>
		</div>
	</div>
</section>

<section class="b-work-row">
	<div class="b-work-row__text">
		<div class="b-row-text-inner">
			<img src="images/work-row_igniting.png" alt="" class="b-work-row-icon">
			<h2 class="b-work-row-header">IGNITING</h2>
			<p>Fired up yet? Women Deliver seeks to inform, inspire, and drive action on behalf of girls and women everywhere. When girls and women are healthy, educated, and empowered they create a ripple effect and yield multiple benefits for the individual, her family, her community, and her nation. Investing in girls and women is not just the right thing to do—it’s a smart investment in achieving a more equitable world.</p>
		</div>
	</div>
	<div class="b-grid b-grid-gif">
		<!-- row -->
		<div class="b-grid-gif__item small">
			<img src="images/grid-gif1.jpg" alt="">
		</div>
		<div class="b-grid-gif__item large">
			<img src="images/grid-gif_L1.jpg" alt="">
		</div>
		<div class="b-grid-gif__item small">
			<img src="images/grid-gif3.jpg" alt="">
		</div>
		<div class="b-grid-gif__item small">
			<img src="images/grid-gif2.jpg" alt="">
		</div>
		<!-- row -->
		<div class="b-grid-gif__item small">
			<img src="images/grid-gif2.jpg" alt="">
		</div>
		<div class="b-grid-gif__item large">
			<img src="images/grid-gif_L2.jpg" alt="">
		</div>
		<!-- row -->
		<div class="b-grid-gif__item small">
			<img src="images/grid-gif_L1.jpg" alt="">
		</div>
		<div class="b-grid-gif__item small">
			<img src="images/grid-gif3.jpg" alt="">
		</div>
		<div class="b-grid-gif__item small">
			<img src="images/grid-gif1.jpg" alt="">
		</div>
		
	</div>
</section>



<section class="b-page-bottom b-page-bottom_type-c">
	<div class="l-content">
		<div class="b-socials b-socials_large">
			<img src="images/bg-socials_w.jpg" alt="">
			<div class="b-socials-col">
				<?php @include('blocks/form-join.php') ?>
			</div>
			<div class="b-socials-col">
				<?php @include('blocks/socials.php') ?>
			</div>
			
		</div>
	</div>
</section>

<?php @include ('blocks/footer.php'); ?>