<?php @include ('blocks/header.php'); ?>

<section class="b-page-img b-page-img_general" style="background-image: url('images/top-page-general.jpg');"></section>
<section class="l-content b-page-content b-page-content_general-detail">
	<aside class="l-content-col-3 b-page-sidebar">
		<?php @include ('blocks/sidebar-list_general.php'); ?>
	</aside>
    <div class="l-content-col-9 b-page-content__inner">
    	<h2>Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua.</h2>
    	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
    	<img src="images/page-img-inner1.jpg" alt="img">
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nullam id dolor id nibh ultricies vehicula ut id elit Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. </p>
    </div>
</section>	
<section class="b-page-bottom b-page-bottom_type-b">
    <div class="l-content">
        <div class="l-content__left">
           
            <div class="b-grid b-grid_page">
                <a class="b-grid__item b-cat">

                    <img class="b-cat__bg" src="images/article-item16.jpg" alt="">
                    <h3 class="b-cat__label mage">Board Members</h3>
                </a>
                <a class="b-grid__item b-cat">
                    <img class="b-cat__bg" src="images/article-item20.jpg" alt="">
                    <h3 class="b-cat__label green">Staff</h3>
                </a>
            </div>
            <div class="l-content-col-main">
                <div class="b-socials b-socials_small">
                    <img src="images/bg-socials_s.jpg" alt="">
                    <div class="b-socials__inner">
                        <?php @include('blocks/socials.php') ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="l-content__right">
            <?php @include('blocks/top-stories.php'); ?>
        </div>
    </div>
</section>

<?php @include ('blocks/footer.php'); ?>