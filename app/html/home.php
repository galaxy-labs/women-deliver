<?php @include ('blocks/header-home.php'); ?>
	<section class="b-home-top" style="">
		<div class="l-content">
			<div class="b-home-top-title">
				<h1>We are a Fierce Advocate for Girls and Women</h1>
				<a href="" class="b-btn b-btn_border b-btn_green">OUR WORK <img src="images/uparrow.svg" class="svg" alt=""></a>
			</div>
		</div>
			
	</section>
	<section class="b-home-updates">
		<div class="b-home-grid " >
			<div class="b-home-grid__left reorder">
				<div class="b-conference-bar" data-order="1">
					<div class="b-text-wrap">
						<h3><span>Women Deliver</span>4th Global Conference</h3>
						<span>16-19 MAY 2016 • COPENHAGEN, DENMARK</span>
					</div>
					<a href="" class="b-btn b-btn_solid b-btn_green">LEARN MORE</a>
				</div>
				<div class="b-article b-article_solid b-article_image" data-order="5">
					<div class="l-article-inner">
						<img src="images/article_img.jpg" alt="">
					</div>
				</div> 

				<div class="b-article b-article_video" data-order="4">
					<a href="" class="b-article-video-btn"></a>
					<img class="b-article__bg" src="images/article-video.jpg" alt="">
					<div class="b-article__text on-white">
						<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
					</div>
				</div> 	 
			</div>
			<div class="b-home-grid__right reorder" >
				<div class="b-article" data-order="2">
					<img class="b-article__bg" src="images/article-item.jpg" alt="">
					<div class="b-article__text on-white">
						<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
					</div>
				</div> 	
				<div class="b-action-callout" data-order="3">
					
					<img src="images/take-action.svg" alt="">
					<span>TAKE ACTION</span>
					<h3>Demand Iran Lift Ban of Wommen’s Sports</h3>
					<a href="">SIGN THE PETITION </a>
					
				</div> 	
			</div>
		</div> 
		<div class="b-home-row ">
			<article class="b-article" >
				<div class="b-article__bg" style="background-image: url('images/article-row.jpg');"></div>
				<!-- <img class="b-article__bg" src="images/article-row.jpg" alt=""> -->
				<div class="b-article__text on-dark big">
					
					<div class="b-article-row-text-inner">
						<span><strong>FEATURED</strong></span>
						<h3>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh dolor sit amet</h3>
					</div>
				</div>
			</article>
		</div>
	</section>
	<section class="b-youth">
		<div class="b-youth__title">
			<h2>Young Leaders</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id dolor id nibh ut id elit duis mollis est non luctus, nisi erat donec sed odio dui.</p>
		</div>
		<ul class="b-person-grid">
			<li class="b-person-grid__item b-person"><img src="images/p-collins.jpg" alt="">
				<a href="" class="b-person-link"><span class="b-person-name">Collins</span></a>
			</li>
			<li class="b-person-grid__item b-person"><img src="images/p-chiamaka.jpg" alt="">
				<a href="" class="b-person-link"><span class="b-person-name">Chiamaka Uzomba</span></a>
			</li>
			<li class="b-person-grid__item b-person"><img src="images/p-chansolinda.jpg" alt="">
				<a href="" class="b-person-link"><span class="b-person-name">Chansolinda</span></a>
			</li>
			<li class="b-person-grid__item b-person"><img src="images/p-dakshitha.jpg" alt="">
				<a href="" class="b-person-link"><span class="b-person-name">Dakshitha</span></a>
			</li>
			<li class="b-person-grid__item b-person"><img src="images/p-adriana.jpg" alt="">
				<a href="" class="b-person-link"><span class="b-person-name">Adriana</span></a>
			</li>
			<li class="b-person-grid__item b-person"><img src="images/p-angela.jpg" alt="">
				<a href="" class="b-person-link"><span class="b-person-name">Angela</span></a>
			</li>
			<li class="b-person-grid__item b-person"><img src="images/p-boris.jpg" alt="">
				<a href="" class="b-person-link"><span class="b-person-name">Boris</span></a>
			</li>

			<li class="b-person-grid__item b-person"><img src="images/p-danica.jpg" alt="">
				<a href="" class="b-person-link"><span class="b-person-name">Danica</span></a>
			</li>
			<li class="b-person-grid__item b-person"><img src="images/p-sofia.jpg" alt="">
				<a href="" class="b-person-link"><span class="b-person-name">Sofia</span></a>
			</li>
			<li class="b-person-grid__item b-person"><img src="images/p-blessing.jpg" alt="">
				<a href="" class="b-person-link"><span class="b-person-name">Blessing</span></a>
			</li>
			<li class="b-person-grid__item b-person"><img src="images/p-umair.jpg" alt="">
				<a href="" class="b-person-link"><span class="b-person-name">Umair</span></a>
			</li>
			<li class="b-person-grid__item b-person"><img src="images/p-salam.jpg" alt="">
				<a href="" class="b-person-link"><span class="b-person-name">Salam</span></a>
			</li>
			<li class="b-person-grid__item b-person"><img src="images/p-temesgen.jpg" alt="">
				<a href="" class="b-person-link"><span class="b-person-name">Temesgen</span></a>
			</li>
			<li class="b-person-grid__item b-person"><img src="images/p-sona.jpg" alt="">
				<a href="" class="b-person-link"><span class="b-person-name">Sona</span></a>
			</li>
		</ul>
		<a href="" class="b-btn b-btn_border b-btn_blue">VIEW THE 2016 CLASS <img src="images/uparrow.svg" alt="" class="svg"></a>
	</section>
	<section class="b-home-bottom">
		<div class="l-content">
			<div class="l-content-col-4" >
				<?php @include('blocks/top-stories.php'); ?>
				
			</div>
			<div class="l-content-col-main" >

				<div class="b-socials b-socials_large">
					<img src="images/bg-socials.jpg" alt="">
					<div class="b-socials__inner">
						<?php @include('blocks/form-join.php') ?>
						<?php @include('blocks/socials.php') ?>
					</div>
				</div>
			</div> 
		</div>
	</section>
<?php @include ('blocks/person-info.php'); ?>
<?php @include ('blocks/footer.php'); ?>