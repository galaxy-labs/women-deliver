<?php @include ('blocks/header.php'); ?>
	
	<section class="b-grid-updates">
		<div class="b-grid-top">
			<!-- <h2>When girls are educated, they unlock their potential</h2> -->
			<h2>Women are disproportionately affected by climate change, yet are key climate adapters and innovators in the fight for a clean, healthy, sustainable world</h2>
			<form class="b-grid-filter-search">
				<input type="text" placeholder="Filter by Issue">
				<a data-link="filter"></a>
			</form>
		</div>
		<div class="b-grid b-grid_default">
			<div class="b-grid__item b-article b-article_solid b-article_image">
				<div class="l-article-inner">
					<img  src="images/article_img.jpg" alt="">
				</div>
			</div>
			<div class="b-grid__item b-article b-article_video">
				<a href="" class="b-article-video-btn"></a>
				<img class="b-article__bg" src="images/article-video.jpg" alt="">
				<div class="b-article__text on-white">
					<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
				</div>
				<div class="b-article__overlay"></div>
			</div>
			<div class="b-grid__item b-article">
				<img class="b-article__bg" src="images/article-item.jpg" alt="">
				<div class="b-article__text on-white">
					<h3>Consectetur adipiscing </h3>
				</div>
			</div>
			<div class="b-grid__item b-article">
				<img class="b-article__bg" src="images/article-item2.jpg" alt="">
				<div class="b-article__text on-white">
					<h3>Sed ut perspiciatis unde om  iste natus error sit volupt accusantium dolorem </h3>
				</div>
			</div> 	 
			<div class="b-grid__item b-grid__item_big b-article">
				<img class="b-article__bg" src="images/article-item_big.jpg" alt="">
				<div class="b-article__text on-dark big">
					<div class="b-article-row-text-inner">
						<span><strong>FEATURED</strong></span>
						<h3>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh dolor sit amet</h3>
					</div>
				</div>
			</div> 	 	 
		
			<div class="b-grid__item b-article">
				<img class="b-article__bg" src="images/article-item3.jpg" alt="">
				<div class="b-article__text on-white">
					<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit </h3>
				</div>
			</div>
			<div class="b-grid__item b-article b-article_solid b-article_image">
				<div class="l-article-inner">
					<img  src="images/article_img1.jpg" alt="">
				</div>
			</div>
			<div class="b-grid__item b-article">
				<img class="b-article__bg" src="images/article-item4.jpg" alt="">
				<div class="b-article__text on-white">
					<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit </h3>
				</div>
			</div>
			<div class="b-grid__item b-article">
				<img class="b-article__bg" src="images/article-item5.jpg" alt="">
				<div class="b-article__text on-white">
					<h3>Lorem ipsum dolor sit amet,</h3>
				</div>
			</div>
			<div class="b-grid__item b-grid__item_big b-article">
				<img class="b-article__bg" src="images/article-item_big2.jpg" alt="">
				<div class="b-article__text on-dark big">
					<div class="b-article-row-text-inner">
						<span><strong>FEATURED</strong></span>
						<h3>Sed ut perspiciatis unde omnis iste natus error sit </h3>
					</div>
				</div>
			</div>
			<div class="b-grid__item b-article">
				<img class="b-article__bg" src="images/article-item6.jpg" alt="">
				<div class="b-article__text on-white">
					<h3>Illo inventore veritatis et qu asi architecto beatae vitae dicta sunt explicabo nemo a</h3>
				</div>
			</div>
			<div class="b-grid__item b-article">
				<img class="b-article__bg" src="images/article-item7.jpg" alt="">
				<div class="b-article__text on-white">
					<h3>Nemo enim ipsam voluptat quia voluptas sit aspernat</h3>
				</div>
			</div>
		</div>
		<a href="" class="b-btn b-btn_mage b-btn_solid">MORE</a>
	</section>

	<section class="b-page-bottom b-page-bottom_type-c">
		<div class="l-content">
			<div class="b-socials b-socials_large">
				<img src="images/bg-socials_w.jpg" alt="">
				<div class="b-socials-col">
					<?php @include('blocks/form-join.php') ?>
				</div>
				<div class="b-socials-col">
					<?php @include('blocks/socials.php') ?>
				</div>
				
			</div>
		</div>
	</section>
<?php @include ('blocks/footer.php'); ?>