<?php @include ('blocks/header.php'); ?>



<section class="b-page-top b-page-top_youth" style="">
	<div class="l-top-inner">
		<h1>Young Leaders</h1>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id dolor id nibh ut id elit duis mollis est non luctus, nisi erat donec sed odio dui.</p>
	</div> 
</section>
<div class="l-content b-youth-content">

	<aside class="l-content-col-3 b-page-sidebar">
		<?php @include ('blocks/sidebar-list.php'); ?>
	</aside>
	<form class="b-grid-filter-search">
		<input type="text" placeholder="Filter">
		<a data-link="filter-person"></a>
	</form>
	<div class="l-content-col-9 ">
		<div class="b-grid-person">
			<div class="b-grid-person__item empty"></div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-adebisi.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Adebisi</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-Adeveline.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Adeveline</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-adriana.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Adriana</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-ahmed.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Ahmed</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-ahmed2.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Ahmed </span>
				</a>
			</div>
			<div class="b-grid-person__item b-person" id="aishoola">
				<img src="images/p-aishoola.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Aishoola</span>
				</a>
				
			</div>
				
			<div class="b-grid-person__item b-person">
				<img src="images/p-aissa.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Aissa</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-alaeddine.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Alaeddine</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-alan.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Alan</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-alisa.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Alisa</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-amadou.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Amadou</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-angela.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Angela</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-angeline.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Angeline</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-anna.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Anna</span>
				</a>
			</div>
			<div class="b-grid-person__item empty"></div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-annick.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Annick</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-anthony.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Anthony</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-appoline.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Appoline</span>
				</a>
			</div>
			<div class="b-grid-person__item empty"></div>
			<div class="b-grid-person__item empty"></div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-asel.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Asel</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-basma.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Basma</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-belachew.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Belachew</span>
				</a>
			</div>
			<div class="b-grid-person__item empty"></div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-blessing.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Blessing</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-boris.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Boris</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-carmen.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Carmen</span>
				</a>
			</div>
			<div class="b-grid-person__item empty"></div>
			<div class="b-grid-person__item empty"></div>
			<div class="b-grid-person__item empty"></div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-catherine.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Catherine</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-cecilia.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Cecilia</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-chansolinda.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Chansolinda</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-chiamaka.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Chiamaka</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-collins.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Collins</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-dakshitha.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Dakshitha</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-danica.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Danica</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-delaine.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Delaine</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-desire.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Desire</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-desmond.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Desmond</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-edith.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Edith</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-elizabeth.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Elizabeth</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-ephriam.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Ephriam</span>
				</a>
			</div>
			<div class="b-grid-person__item empty"></div>
			<div class="b-grid-person__item empty"></div>
			<div class="b-grid-person__item empty"></div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-evelyn.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Evelyn</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-florence.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Florence</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-francis.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Francis</span>
				</a>
			</div>
			<div class="b-grid-person__item empty"></div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-francis2.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Francis</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-Franklin.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Franklin</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-gbemisola.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Gbemisola</span>
				</a>
			</div>
			<div class="b-grid-person__item empty"></div>
			<div class="b-grid-person__item empty"></div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-genesis.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Genesis</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-georgeleen.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Georgeleen</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-georgiana.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Georgiana</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-Hanifatur.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Hanifatur</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-hashim.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Hashim</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-helena.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Helena</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-inaam.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Inaam</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-irene.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Irene</span>
				</a>
			</div>
			<div class="b-grid-person__item b-person">
				<img src="images/p-isaac.jpg" alt="">
				<a href="" class="b-person-link">
					<span class="b-person-name">Isaac</span>
				</a>
			</div>
		</div>
		<a href="" class="b-btn b-btn_border b-btn_blue"> MORE<img src="images/uparrow.svg" alt="" class="svg"></a>
	</div>

</div>
<section class="b-page-bottom b-page-bottom_type-c">
	<div class="l-content">
		<div class="b-socials b-socials_large">
			<img src="images/bg-socials_w.jpg" alt="">
			<div class="b-socials-col">
				<?php @include('blocks/form-join.php') ?>
			</div>
			<div class="b-socials-col">
				<?php @include('blocks/socials.php') ?>
			</div>
			
		</div>
	</div>
</section>

<?php @include ('blocks/person-info.php'); ?>
<?php @include ('blocks/footer.php'); ?>