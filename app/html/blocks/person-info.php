<!-- Person Info -->
<div class="b-person-dropdown" data-person="aishoola">
	<div class="l-drop-left">
		<div class="b-pesron-card">
			<img class="b-person-card__img" src="images/article-item8.jpg" alt="">
			<h4 class="b-person-card__text">Nullam id dolor id nibh ultricies vehicula ut id elit. Aenean lacinia bibendum nulla sed consectetur. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</h3>
		</div>
	</div>
	<div class="l-drop-right">
		<div class="b-person-info">
			<a class="b-person-info__closer">&#xf00d;</a>
			<h3 class="b-person-info__header">Aishoola Aisaeva</h3>
			<ul class="b-person-info__tag-list">
				<li><a href="">Issue TITLE HERE </a></li>
				<li><a href="">Region NAME HERE</a></li>         
			</ul>
			<ul class="b-person-info__media-list">
				<li><a class="b-person-media-link b-person-media-link_twitter" href="">@aishoolaa</a></li>
				<li><a class="b-person-location-link" href="">Country</a></li>
			</ul>
			<div class="b-person-info__text">
				<p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vesti bulum. Voluptate id ligula porta felis euismod semper. Integer posuere erat a ante ven enatis dapibus posuere velit aliquet. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
			</div>
			<div class="b-person__related-content">
				<div class="b-article b-article_video">

					<a href="" class="b-article-video-btn"></a>
					<img class="b-article__bg" src="images/article-video.jpg" alt="">
					<div class="b-article__text on-white">
						<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="b-person-dropdown" data-person="test">
	<div class="l-drop-left">
		<div class="b-pesron-card">
			<img class="b-person-card__img" src="images/article-item8.jpg" alt="">
			<h4 class="b-person-card__text">Nullam id dolor id nibh ultricies vehicula ut id elit. Aenean lacinia bibendum nulla sed consectetur. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</h3>
		</div>
	</div>
	<div class="l-drop-right">
		<div class="b-person-info">
			<a class="b-person-info__closer">&#xf00d;</a>
			<h3 class="b-person-info__header">Test Card</h3>
			<ul class="b-person-info__tag-list">
				<li><a href="">Issue TITLE HERE </a></li>
				<li><a href="">Region NAME HERE</a></li>         
			</ul>
			<ul class="b-person-info__media-list">
				<li><a class="b-person-media-link b-person-media-link_twitter" href="">@aishoolaa</a></li>
				<li><a class="b-person-location-link" href="">Country</a></li>
			</ul>
			<div class="b-person-info__text">
				<p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vesti bulum. Voluptate id ligula porta felis euismod semper. Integer posuere erat a ante ven enatis dapibus posuere velit aliquet. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
			</div>
			<div class="b-person__related-content">
				<div class="b-article b-article_video">
					
					<a href="" class="b-article-video-btn"></a>
					<img class="b-article__bg" src="images/article-video.jpg" alt="">
					<div class="b-article__text on-white">
						<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="b-person-dropdown" data-person="Katja_Iverson">
	<div class="l-drop-left">
		<div class="b-pesron-card">
			<img class="b-person-card__img" src="images/p-staff-katja-iverson.jpg" alt="">
			<h4 class="b-person-card__text">Nullam id dolor id nibh ultricies vehicula ut id elit. Aenean lacinia bibendum nulla sed consectetur. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</h3>
		</div>
	</div>
	<div class="l-drop-right">
		<div class="b-person-info">
			<a class="b-person-info__closer">&#xf00d;</a>
			<h3 class="b-person-info__header">Katja Iverson</h3>
			<ul class="b-person-info__tag-list">
				<li><a href="">CHIEF EXECUTIVE OFFICER </a></li>
			</ul>
			<ul class="b-person-info__media-list">
				<li><a class="b-person-media-link b-person-media-link_twitter" href="">@katjaiverson</a></li>
			</ul>
			<div class="b-person-info__text">
				<p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vesti bulum. Voluptate id ligula porta felis euismod semper. Integer posuere erat a ante ven enatis dapibus posuere velit aliquet. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>

				<p>Maecenas sed diam eget risus varius blandit sit amet non magna. Cras mattis consectetur purus sit amet fermentum. Vestibulum id ligula porta felis euismod semper. Donec id elit non mi porta gravida at eget metus. Maecenas faucibus mollis interdum. Donec sed odio dui. Donec sed odio dui. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Nulla vitae elit libero, a pharetra augue.</p>

				<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec ullamcorper nulla non metus auctor fringilla. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Sed posuere consectetur est at lobortis. Cras justo odio, dapibus ac facilisis in, egestas eget quam ...</p>
			</div>
			<div class="b-person__related-content">
				<a href="" class="b-btn b-btn_border b-btn_get"><span>DOWNLOAD</span> FULL BIO &amp; HEADSHOT<img src="images/uparrow.svg" class="svg" alt=""></a>
			</div>
		</div>
	</div>
</div>