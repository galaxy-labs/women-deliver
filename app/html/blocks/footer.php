    <footer class="b-footer">
        <nav class="b-footer-nav">
            <ul class="b-footer-nav-list">
                <li>
                    <h4 href=""><a class="b-footer-link-type-a" href="">THE ISSUES</a></h4>
                    <ul class="b-footer-link-list">
                        <li><a href="">Sexual Reproductive Health &amp; Rights</a></li>
                        <li><a href="">Maternal &amp; Newborn Health</a></li>
                        <li><a href="">Gender Equality</a></li>
                        <li><a href="">Cross-Sector Solutions</a></li>
                    </ul>
                </li>
                <li>
                    <h4 href=""><a class="b-footer-link-type-a" href="">OUR WORK</a></h4>
                    <ul class="b-footer-link-list">
                        <li><a href="">Who We Are</a></li>
                        <li><a href="">What We Do</a></li>
                    </ul>
                </li>
                <li>
                    <h4 href=""><a class="b-footer-link-type-a" href="">DELIVER FOR GOOD</a></h4>
                    <ul class="b-footer-link-list">
                        <li><a href="">Sub item 1</a></li>
                    </ul>
                </li>
                <li>
                    <h4 href=""><a class="b-footer-link-type-a" href="">CONFERENCE</a></h4>
                    <ul class="b-footer-link-list">
                        <li><a href="">Conference 2016</a></li>
                        <li><a href="">Past Conferences</a></li>
                    </ul>
                </li>
                <li>
                    <h4 href=""><a class="b-footer-link-type-a" href="">YOUTH</a></h4>
                    <ul class="b-footer-link-list">
                        <li><a href="">Featured Stories</a></li>
                        <li><a href="">Class of 2016</a></li>
                        <li><a href="">Past Classes</a></li>
                    </ul>
                </li>
                <li>
                    <h4 href=""><a class="b-footer-link-type-b" href="">ABOUT US</a></h4>
                    <ul class="b-footer-link-list">
                        <li><a href="">Board Members</a></li>
                        <li><a href="">Staff</a></li>
                        <li><a href="">Careers</a></li>
                        <li><a href="">Corporate Partnerships</a></li>
                        <li><a href="">Annual Reports</a></li>
                    </ul>
                </li>
                <li>
                    <h4 href=""><a class="b-footer-link-type-b" href="">NEWS</a></h4>
                    <ul class="b-footer-link-list">
                        <li><a href="">Infographic 1</a></li>
                        <li><a href="">Infographic 2</a></li>
                    </ul>
                </li>
                <li>
                    <h4 href=""><a class="b-footer-link-type-b" href="">RESOURCES</a></h4>
                    <ul class="b-footer-link-list">
                       <li><a href="">Infographic 1</a></li>
                        <li><a href="">Infographic 2</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <nav class="b-footer-subnav">
            <a href="" class="b-footer-logo"><img src="images/women-deliver-logo-green.svg" alt=""></a>
            <ul class="b-footer-info">
                <li><a href="">Privacy Policy</a></li>
                <li><a href="">Terms and Conditions  </a></li>
                <li><a href="">Language  </a></li>
                <li>Photos &copy;Mark Tuschman</li>
            </ul>
            <div class="b-footer-btn-set">
                <a href="" class="b-btn b-btn_gold b-btn_solid b-footer-btn b-btn_contact">Contact <i class="b-btn-icon"></i></a>
                <a href="" class="b-btn b-btn_green b-btn_solid b-footer-btn b-btn_donate">Donate <i class="b-btn-icon"></i></a>
            </div>
                
        </nav>
        
    </footer>
    <div class="b-footer-mobile">
        <ul class="b-footer-info">
            <li><a href="">Privacy Policy</a></li>
            <li><a href="">Terms and Conditions  </a></li>
            <li>Photos &copy;Mark Tuschman</li>
        </ul>
    </div>
    <div class="b-modal b-search-modal" data-id="search"> 
        <form action="" class="b-search-form-mini">
            <label for=""></label>
            <input type="text" placeholder="Search the site">
            <button type="submit"><img src="images/uparrow.svg" class="svg" alt=""></button>
        </form>
    </div>

    <div class="b-modal b-filter-modal b-filter-options b-filter-stories" data-id="filter-stories">
        <form action="" class="b-filter-form">
            <h3 class="b-mobile-filter-header">Filter</h3>
            <fieldset>
                <h3 class="b-filter-options__title b-filter-options__title_region">Filter By Region</h3>
                <a class="b-modal-closer b-filter-options__closer"></a>
                <ul class="b-region-filter-list">
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Region 1</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Region 2</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Region 3</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Region 4</span></label>
                    </li>
                     <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Region 5</span></label>
                    </li>
                     <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Region 6</span></label>
                    </li>
                     <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Region 7</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Region 8</span></label>
                    </li>
                </ul>
                <h3 class="b-filter-options__title b-filter-options__title_issue">Filter By Issue</h3>
                <ul class="b-issue-filter-list">
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Issue 1</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Issue 2</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Issue 3</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Issue 4</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Issue 5</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Issue 6</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Issue 7</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Issue 8</span></label>
                    </li>
                </ul>
                <h3 class="b-filter-options__title b-filter-options__title_type">Filter By Type</h3>
                <ul class="b-type-filter-list">
                    <li><a href="" class="b-filter-link b-filter-link_content"><img src="images/uparrow.svg" class="svg" alt=""> Women Deliver Content    </a></li>
                    <li><a href="" class="b-filter-link b-filter-link_graph">Infographics   </a></li>
                    <li><a href="" class="b-filter-link b-filter-link_video"><img src="images/videosicon.svg" class="svg" alt="">Videos  </a></li>
                    <li><a href="" class="b-filter-link b-filter-link_podcast">Podcasts   </a></li>
                    <li><a href="" class="b-filter-link b-filter-link_article">Articles</a></li>
                </ul>
            </fieldset>
            <button class="b-btn b-btn_solid b-btn_white">APPLY FILTER</button>
        </form>
    </div>
     <div class="b-modal b-filter-modal b-filter-options b-filter-stories" data-id="filter-person">
        <form action="" class="b-filter-form">
            <h3 class="b-mobile-filter-header">Filter</h3>
            <fieldset>

                <h3 class="b-filter-options__title b-filter-options__title_region">Filter By Region</h3>
                <a class="b-modal-closer b-filter-options__closer"></a>
                <ul class="b-region-filter-list">
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Region 1</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Region 2</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Region 3</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Region 4</span></label>
                    </li>
                     <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Region 5</span></label>
                    </li>
                     <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Region 6</span></label>
                    </li>
                     <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Region 7</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Region 8</span></label>
                    </li>
                </ul>
                <h3 class="b-filter-options__title b-filter-options__title_issue">Filter By Issue</h3>
                <ul class="b-issue-filter-list">
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Issue 1</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Issue 2</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Issue 3</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Issue 4</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Issue 5</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Issue 6</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Issue 7</span></label>
                    </li>
                    <li> 
                        <input type="checkbox" class="b-filter-group-item">
                        <label for=""><span>Issue 8</span></label>
                    </li>
                </ul>
            </fieldset>
            <button class="b-btn b-btn_solid b-btn_white">APPLY FILTER</button>
        </form>
    </div>
    <div class="b-modal b-filter-modal b-filter-options" data-id="filter">
        <form action="" class="b-filter-form">
            <h3 class="b-filter-options__title b-filter-options__title_issue">Filter By Issue</h3>
            <a class="b-modal-closer b-filter-options__closer"></a>
            <ul class="b-filter-list">
                <li> 
                    <input type="checkbox" class="b-filter-group-item">
                    <label for=""><span>Sexual Reproductive Health &amp; Rights</span></label>
                    <ul class="b-filter-sub-list">
                        <li>
                            <input type="checkbox" class="b-filter-item">
                            <label for=""><span>Contraception</span></label> 
                        </li>
                        <li>
                            
                            <input type="checkbox" class="b-filter-item">
                            <label for="">
                                <span>Unsafe Abortion</span>
                            </label>
                        </li>
                        <li>
                            
                            <input type="checkbox" class="b-filter-item">
                            <label for="">
                                <span>Cervical Cancer</span>
                            </label>
                        </li>
                        <li>
                            
                            <input type="checkbox" class="b-filter-item">
                            <label for="">
                                <span>HIV &amp; AIDS</span>
                            </label>
                        </li>
                    </ul>
                </li>
                <li> 
                    <input type="checkbox" class="b-filter-group-item">
                    <label for=""><span>Maternal &amp; Newborn Health</span></label>
                    <ul class="b-filter-sub-list">
                        <li>
                            <input type="checkbox" class="b-filter-item">
                            <label for=""><span>Nutrition </span></label> 
                        </li>
                        <li>
                            
                            <input type="checkbox" class="b-filter-item">
                            <label for="">
                                <span>Gestational Diabetes</span>
                            </label>
                        </li>
                        <li>
                            
                            <input type="checkbox" class="b-filter-item">
                            <label for="">
                                <span>Pregnancy Care</span>
                            </label>
                        </li>
                    </ul>
                </li>
                <li> 
                    <input type="checkbox" class="b-filter-group-item">
                    <label for=""><span>Gender Equality</span></label>
                    <ul class="b-filter-sub-list">
                        <li>
                            <input type="checkbox" class="b-filter-item">
                            <label for=""><span>Education </span></label> 
                        </li>
                        <li>
                            
                            <input type="checkbox" class="b-filter-item">
                            <label for="">
                                <span>Economic Empowerment</span>
                            </label>
                        </li>
                        <li>
                            
                            <input type="checkbox" class="b-filter-item">
                            <label for="">
                                <span>Gender-Based Violence</span>
                            </label>
                        </li>
                        <li>
                            
                            <input type="checkbox" class="b-filter-item">
                            <label for="">
                                <span>Political Participation</span>
                            </label>
                        </li>
                    </ul>
                </li>
                <li> 
                    <input type="checkbox" class="b-filter-group-item">
                    <label for=""><span>Cross-Sector Solutions</span></label>
                    <ul class="b-filter-sub-list">
                        <li>
                            <input type="checkbox" class="b-filter-item">
                            <label for=""><span>Access to Resources </span></label> 
                        </li>
                        <li>
                            
                            <input type="checkbox" class="b-filter-item">
                            <label for="">
                                <span>Water &amp; Sanitation</span>
                            </label>
                        </li>
                        <li>
                            
                            <input type="checkbox" class="b-filter-item">
                            <label for="">
                                <span>Climate &amp; Environment</span>
                            </label>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="b-filter-list-bottom">
                <li><a href="" class="b-filter-link b-filter-link_content"><img src="images/uparrow.svg" class="svg" alt=""> Women Deliver Content    </a></li>
                <li><a href="" class="b-filter-link b-filter-link_graph">Infographics   </a></li>
                <li><a href="" class="b-filter-link b-filter-link_video"><img src="images/videosicon.svg" class="svg" alt="">Videos  </a></li>
                <li><a href="" class="b-filter-link b-filter-link_podcast">Podcasts   </a></li>
                <li><a href="" class="b-filter-link b-filter-link_article">Articles</a></li>
            </ul>
            <button class="b-btn b-btn_solid b-btn_white">APPLY FILTER</button>
        </form>
    </div>
</body>
</html>