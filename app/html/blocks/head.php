<!DOCTYPE html>

<html>

<head >
   <!--  <meta property="og:type" content="website"/>
    
    <meta property="og:image" content="http://dev-gl.ru/current/WD/images/article-item.jpg" />
    
    <meta property="og:title" content="test"/>
    <meta property="og:url" content="http://dev-gl.ru/current/WD/home.php"/>
 -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
    <title>WD</title>

    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900' rel='stylesheet' type='text/css'>
    <link href='css/style.css' rel="stylesheet" type='text/css'>
    <script src='js/plugins.js'></script> 
    <script src='js/main.js'></script>
</head>
<?php
    $link = $_SERVER['PHP_SELF'];
    $link_array = explode('/',$link);
    $page = end($link_array);
    $body_class = preg_replace('/\\.[^.\\s]{3,4}$/', '', $page);
?>
<body class="<?php echo $body_class; ?>">
    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->