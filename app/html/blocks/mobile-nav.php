<!-- MOBILE NAV -->
<nav class="b-nav-mobile">
	<div class="b-nav-mobile__top">
		<ul class="b-mobile-top-list">
			<li>
				<a href="">THE ISSUES</a>
				
			</li>
			<li>
				<a href="">CONFERENCE</a>
			</li>
			<li>
				<a class="b-mobile-search-link" data-link="search"></a>
			</li>
		</ul>
	</div>
	<div class="b-nav-mobile__bottom">
		<span class="b-header-logo-mobile">
			<a href="http://dev-gl.ru/current/WD/home.php" ></a>
		</span>
        <a class="b-mobile-nav-btn"></a>
	</div>
	<div class="b-nav-mobile__back">
		<span class="b-mobile-nav-closer"><a></a></span>
		<ul class="b-mobile-list">
			<li class="b-mobile-list__item b-mobile-list__item_large parent">
				<a href="">The Issues <span class="b-icon-closer"></span></a>
				<div class="b-mobile-sub-menu">
					<ul class="b-mobile-sub-list">
						<li class="b-sub-item">
							<a href="">
								<img src="images/nav-thumb-1.jpg">
								<span class="b-sub-item-label">Sexual Reproductive Health &amp; Rights</span>
							</a>		
						</li>
						<li class="b-sub-item">
							<a href="">
								<img src="images/nav-thumb-2.jpg">
								<span class="b-sub-item-label">Maternal &amp; Newborn Health</span>
							</a>		
						</li>	
						<li class="b-sub-item">
							<a href="">
								<img src="images/nav-thumb-3.jpg">
								<span class="b-sub-item-label">Gender Equality</span>
							</a>		
						</li>	
						<li class="b-sub-item">
							<a href="">
								<img src="images/nav-thumb-4.jpg">
								<span class="b-sub-item-label">Cross-Sector Solutions</span>
							</a>		
						</li>					
					</ul>
				</div>
			</li>
			<li class="b-mobile-list__item b-mobile-list__item_large parent">
				<a href="">Our Work <span class="b-icon-closer"></span></a>
				<div class="b-mobile-sub-menu">
					<ul class="b-mobile-sub-list">
						<li class="b-sub-item">
							<a href="">
								<span class="b-sub-item-label">Who We Are</span>
							</a>		
						</li>
						<li class="b-sub-item">
							<a href="">
								<span class="b-sub-item-label">What We Do</span>
							</a>		
						</li>	
					</ul>
				</div>
			</li>
			<li class="b-mobile-list__item b-mobile-list__item_large"><a href="">Campaign</a></li>
			<li class="b-mobile-list__item b-mobile-list__item_large"><a href="">Conference</a></li>
			<li class="b-mobile-list__item b-mobile-list__item_large"><a href="">Youth</a></li>
			<li class="b-mobile-list__item"><a href="">About Us</a></li>
			<li class="b-mobile-list__item"><a href="">News</a></li>
			<li class="b-mobile-list__item"><a href="">Resources</a></li>
		</ul>
		<div class="b-btn-set b-btn-set_2">
			<a href="" class="b-btn b-btn_brown b-btn_solid b-btn_contact">CONTACT</a>
			<a href="" class="b-btn b-btn_gold b-btn_solid b-btn_donate">DONATE</a>
		</div>
		<div class="b-mobile-nav-form">
			<?php @include('blocks/form-updates.php'); ?>
		</div>
		
	</div>
</nav>