<div class="b-top-stories">
	<div class="b-top-stories-title">
		<h3 class="b-top-stories-heading">Top Stories</h3>
		<a href="" class="b-all-stories-link">View All</a>
	</div>
		
	<ul class="b-story-list">
		<li class="b-story-list__item">
			<a href="">
				<img src="images/story-thumb1.jpg" alt="">
				<div class="b-story-text">
					<p><span class="b-story-label ">ISSUE</span></p>
					<p>Lorem ipsum dolor sit amet, consect etur adipiscing elit. Nullam id dolor id nibh ut id elit duis </p>
				</div>
			</a>
		</li>
		<li class="b-story-list__item">
			<a href="">
				<div class="b-story-text">
					<p><span class="b-story-label ">ISSUE</span></p>
					<p>Lorem ipsum dolor sit amet, consect etur adipiscing elit. Nullam id dolor id nibh ut id elit duis </p>
				</div>
					
			</a>
		</li>
		<li class="b-story-list__item">
			<a href="">
				<img src="images/story-thumb2.jpg" alt="">
				
				<div class="b-story-text">
					<p><span class="b-story-label ">ISSUE</span></p>
					<p>Lorem ipsum dolor sit amet, co nsectetur adipiscing elit Nullam </p>
				</div>
			</a>
		</li>
		<li class="b-story-list__item">
			<a href="">
				<img src="images/story-thumb3.jpg" alt="">
				
				<div class="b-story-text">
					<p><span class="b-story-label ">ISSUE</span></p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id dolor id nibh ut id elit duis</p>
				</div>
			</a>
		</li>
		<!-- <li class="b-story-list__item">
			<a href="">
				<div class="b-story-text">
					<p><span class="b-story-label b-story-label_mage">ISSUE</span></p>
					<p>Lorem ipsum dolor sit amet, consect etur adipiscing elit. Nullam id dolor id nibh ut id elit duis </p>
				</div>
			</a>
		</li> -->
	</ul>
</div>