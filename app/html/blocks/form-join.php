<!-- FORM JOIN -->
<div class="b-join-form-wrap">
	<form action="" class="b-simple-form">
		<h3>Join the Community</h3>
		<p>Sign up to recieve updates from Women Deliver!</p>
		<div class="b-field-box white">
			<label for=""></label>
			<input type="text" placeholder="Enter your email">
			<button><img src="images/uparrow.svg" class="svg" alt=""></button>
		</div>
	</form>
	<!-- <a class="b-form-link" href="">TERMS</a>
	<a class="b-form-link" href="">PRIVACY POLICY</a> -->
</div> 