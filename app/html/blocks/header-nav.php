<!-- HEADER NAVIGATION -->

<nav class="b-header-nav">
    <span class="b-header-logo">
        <a href="http://dev-gl.ru/current/WD/home.php">
            <!-- <img src="images/logo-header.png" alt=""> -->
        </a>
    </span> 
    <section class="b-nav-top">  
        <ul class="b-nav-list b-nav-list_sub">
            <li><a class="b-sub-nav-link" href="">About Us</a></li>
            <li><a class="b-sub-nav-link" href="">News</a></li>
            <li><a class="b-sub-nav-link" href="">Resources  </a></li> 
            <li class="b-nav-item-lang">
                <a class="b-sub-nav-link b-sub-nav-link_lang" href="">Language </a>
                <ul class="b-sub-nav-drop">
                    <li><a href="">Language 1</a> </li>
                    <li><a href="">Language 2</a></li>
                </ul> 
            </li>
            <li><a class="b-btn b-btn_solid b-btn_green b-btn_donate" href="">DONATE</a></li>
        </ul>
        <ul class="b-nav-list b-nav-list_prime">
            <li>
                <a class="b-nav-link" href="">THE ISSUES </a>
                <div class="b-nav-drop">
                    <ul class="b-nav-drop__list">
                        <li class="b-nav-drop-item">
                            <a class="b-drop-item-link" href="">
                                <img src="images/nav-thumb-1.jpg" alt="">
                                <span class="b-drop-item-label">
                                    <h4>Sexual Reproductive Health &amp; Rights</h4>
                                </span>
                            </a>
                        </li>
                        <li class="b-nav-drop-item">
                            <a class="b-drop-item-link" href="">
                                <img src="images/nav-thumb-2.jpg" alt="">
                                <span class="b-drop-item-label">
                                    <h4>Maternal &amp; Newborn Health</h4>
                                </span>
                            </a>
                        </li>
                        <li class="b-nav-drop-item">
                            <a class="b-drop-item-link" href="">
                                <img src="images/nav-thumb-3.jpg" alt="">
                                <span class="b-drop-item-label">
                                    <h4>Gender Equality</h4>
                                </span>
                            </a>
                        </li>
                        <li class="b-nav-drop-item">
                            <a class="b-drop-item-link" href="">
                                <img src="images/nav-thumb-4.jpg" alt="">
                                <span class="b-drop-item-label">
                                    <h4>Cross-Sector Solutions</h4>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>  
            </li>
            <li><a class="b-nav-link" href="">OUR WORK  </a></li>
            <li><a class="b-nav-link" href="">DELIVER FOR GOOD </a></li>
            <li><a class="b-nav-link" href="">CONFERENCE  </a>
            <li><a class="b-nav-link" href="">YOUTH  </a>
            <li><a class="b-nav-link b-nav-link_search" data-link="search"></a></li>
        </ul>
    </section>
</nav>